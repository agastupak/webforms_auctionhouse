﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SqlServer.Server;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.UserControls;
using WebApplication5.View;

namespace WebApplication5
{
    public partial class ShowDetails : BasePage<ShowDetailsPresenter>, IShowDetailsView
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }


        protected void btnSetNewPrize_OnClick(object sender, EventArgs e)
        {
            Presenter.SetPrize();   
        }

        protected void btnAddToFavourite_OnClick(object sender, EventArgs e)
        {
            Presenter.AddToFavourite();
        }

        public aspnet_User AspnetUser { get; set; }
        public string Currency => ddlCurrency.SelectedValue;
        public int AuctionId => int.Parse(Request.QueryString["id"]);

        public decimal Prize
        {
            get { return decimal.Parse(txtbSetNewPrize.Text); }
            set { txtbSetNewPrize.Text = value.ToString(); }
        }
        public void ShowAuction(Auction auction)
        {
            lblUserName.Text = auction.aspnet_User.UserName;
            lblAuctionName.Text = auction.Name;
            lblDescription.Text = auction.Description;
            NewPrize = auction.Prize;
            //txtbSetNewPrize.Attributes.Add("placeholder", (NowaCena + 1).ToString());
        }

        public void SetLinks()
        {
            HyperLink1.NavigateUrl = String.Format(HyperLink1.NavigateUrl, AspnetUser.UserName);
            imgAuction.ImageUrl = Page.ResolveUrl("ImageHandler.ashx?id=") + AuctionId;
        }

        public bool IsLogged { get; set; }
        public void NotLogged()
        {
            if (IsLogged) return;
            btnSetNewPrize.Visible = false;
            btnAddToFavourite.Visible = false;
            txtbSetNewPrize.Visible = false;
            ddlCurrency.Visible = false;
        }

        public decimal NewPrize { get { return decimal.Parse(lblPrize.Text); } set { lblPrize.Text = value.ToString(); } }
        public void PrizeAlert(string alert)
        {
            lblPrizeAlert.Text = alert;
        }

        public string ToFavourite { set { lblAddToFavourite.Text = value; }}


        protected void ddlCurrency_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            PrizeAlert("");
            Presenter.ChangeCurrency();
        }

    }
}