﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowDetails.aspx.cs" MasterPageFile="~/MasterPageOne.Master" Inherits="WebApplication5.ShowDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CategoryMenu" runat="server">

    <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="Users/User.aspx?name={0}"> Sprzedawca</asp:HyperLink>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br/>
    <asp:Image runat="server" ID="imgAuction"/>
    <br/>
    <asp:Label runat="server" ID="lblUserName"></asp:Label>
    <br />
    <asp:Label runat="server" ID="lblAuctionName"></asp:Label>
    <br />
    <asp:Label runat="server" ID="lblPrize" Text="Obecna cena"></asp:Label>
    <br />
    <asp:Label runat="server" ID="lblDescription"></asp:Label>
    <br />
    
   
    <asp:TextBox runat="server" ID="txtbSetNewPrize"></asp:TextBox>
    <asp:RegularExpressionValidator ID="revPrize" runat="server"  ControlToValidate="txtbSetNewPrize" ErrorMessage="zly format ceny" ValidationExpression="^[0-9]{1,9}([.,][0-9]{1,2})?$"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="rvPrize" runat="server"  Display="Dynamic" ControlToValidate="txtbSetNewPrize" ErrorMessage="Musisz podac swoja propozycje ceny"></asp:RequiredFieldValidator>
    <asp:Button runat="server" ID="btnSetNewPrize" Text="Licytuj" CausesValidation="True" OnClick="btnSetNewPrize_OnClick" />
  <asp:Label runat="server" ID="lblPrizeAlert"></asp:Label>
    <br/>
    <asp:DropDownList runat="server" ID="ddlCurrency" OnSelectedIndexChanged="ddlCurrency_OnSelectedIndexChanged" AutoPostBack="True" AppendDataBoundItems="True">
        <asp:ListItem Text="PLN" Value="PLN"></asp:ListItem>
        <asp:ListItem Text="EUR" Value="EUR"></asp:ListItem>
        <asp:ListItem Text="USD" Value="USD"></asp:ListItem>
        </asp:DropDownList>

    
     <asp:Button runat="server" ID="btnAddToFavourite" CausesValidation="False" Text="Dodaj do ulubionych" OnClick="btnAddToFavourite_OnClick"/>
    <asp:Label runat="server" ID="lblAddToFavourite"></asp:Label>
    
    
    <asp:Label runat="server" ID="lblCenaNowa"></asp:Label>
</asp:Content>
