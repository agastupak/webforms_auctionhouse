﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class UserInfoPresenter : BasePresenter<IUserInfoView>

    {
        public override void Load(bool isPostBack)
        {
            base.Load(isPostBack);
            if (!isPostBack)
            {
                View.UserList = UserList();
                View.UserInfoDataBind();
            }

        }

        public List<aspnet_User> UserList()
        {
            IUserRepository userRepository = new UserRepository();
            return userRepository.UserList();
        }

        //public void DeleteUser()
        //{
        //   IUserRepository userRepository = new UserRepository();
        //   userRepository.DeleteUser(View.UserId);
        //}
    }
}