﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public abstract class BasePresenter
    {
        public abstract void Initialize(IBaseView view);

        public virtual void PreLoad(bool isPostBack)
        {
            
        }

        public virtual void Load(bool isPostBack)
        {
            
        }

        public virtual void PostLoad(bool isPostBack)
        {
            
        }

        public virtual void PreRender(bool isPostBack)
        {
            
        }
    }

    public class BasePresenter<T> : BasePresenter
    {
        public T View { get; private set; }

        public override void Initialize(IBaseView view)
        {
            View = (T)view;
        }
       
    }
}