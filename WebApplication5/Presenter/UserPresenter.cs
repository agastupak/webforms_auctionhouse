﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class UserPresenter : BasePresenter<IUserView>
    {
        public override void Load(bool isPostBack)
        {
            base.Load(isPostBack);

            if (!isPostBack)
            {
                if (View.UserName != null)
                {
                    View.UserDataBind();
                }
            }

        }

        public List<Auction> GetAuctionsByUserName()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAuctionsByUserName(View.UserName);
        }

        public List<Opinion> GetOpinionsByUserName()
        {
            IOpinionRepository opinionRepository = new OpinionRepository();
            return opinionRepository.GetOpinionsByUserName(View.UserName);
        }
    }
}