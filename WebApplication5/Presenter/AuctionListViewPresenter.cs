﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.UserControls;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class AuctionListViewPresenter:BasePresenter<IAuctionListView>
    {

        public void Bind()
        {
            IEnumerable<Auction> auctions = Enumerable.Empty<Auction>();

            switch (View.Mode)
            {
                    case AuctionListView.ALVDisplayMode.All:
                    auctions = GetAllAuctions();
                    break;

                    case AuctionListView.ALVDisplayMode.UserAuctions:
                    auctions = GetUserAuctions();
                    View.HideSearchBox();
                    break;

                    case AuctionListView.ALVDisplayMode.Favourite:
                    auctions = GetFavourite().Select(f=> f.Auction);
                    View.HideSearchBox();
                    break;

                    case AuctionListView.ALVDisplayMode.FromCategory:
                    auctions = GetAuctionsFromCategory();
                    break;
            }

            View.BindAuctions(auctions);
        }

        public List<Favourite> GetFavourite()
        {
           IFavouriteRepository repository = new FavouriteRepository();
            IUserRepository userRepository = new UserRepository();
            Guid userId = userRepository.GetUserIdByUserName(View.Name);
            return repository.GetFavAuctions(userId);
        }

        public List<Auction> GetUserAuctions()
        {
           IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAuctionsByUserName(View.Name);
        }

        public List<Auction> GetAllAuctions()
        { 
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAll();
        }

        public List<Auction> GetAuctionsFromCategory()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAuctionsByCategoryName(View.Name);
        }

        public void GetAuctionsContains()
        {
            var auctionRepo = RepoFactory.Create<IAuctionRepository>();
            // var auctionRepo = RepoFactory.Create<IAuctionRepository>();
            View.BindAuctions(auctionRepo.GetAuctionsContains(View.Keyword, View.FilterMode));
        }
    }
}