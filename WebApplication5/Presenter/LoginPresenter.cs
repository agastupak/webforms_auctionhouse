﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class LoginPresenter : BasePresenter<ILoginView>
    {
        public void Login()
        {
            var userRepo = RepoFactory.Create<IUserRepository>();

            string errorMessage = userRepo.LogIn(View.UserName, View.Password, View.RememberMe);
            View.SetErrorLabel = errorMessage;
        }
    }
}