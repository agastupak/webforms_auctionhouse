﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.currencyconverter;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class ShowDetailsPresenter:BasePresenter<IShowDetailsView>
    {
        public override void Load(bool isPostBack)
        {
            IAuctionRepository repo = new AuctionRepository();
            IUserRepository userRepository = new UserRepository();
            View.IsLogged = userRepository.IsLogged();
            View.NotLogged();
            base.Load(isPostBack);

            var auction = repo.GetAuctionById(View.AuctionId);
            View.AspnetUser = auction.aspnet_User;
            _prizeInPLN = auction.Prize;

            if (!isPostBack)
            {
                
                View.ShowAuction(auction);
                View.SetLinks();
            }  
        }

        private static decimal _prizeInPLN;

        public void SetPrize()
        {
           IAuctionRepository auctionRepository = new AuctionRepository();
            var auction = auctionRepository.GetAuctionById(View.AuctionId);

            if (auctionRepository.SetPrize(View.AuctionId, View.Prize, View.Currency))
            {
                View.PrizeAlert("Licytujesz aukcje");
                View.NewPrize = auction.Prize;
            }
            else
            {
                View.PrizeAlert("Podaj wieksza oferte");
            }
            
           
        }

        public void AddToFavourite()
        {
            IFavouriteRepository repository = new FavouriteRepository();
            IUserRepository userRepository = new UserRepository();
            Guid userId = userRepository.GetUserId();
            if (repository.AddFavourite(userId, View.AuctionId))
            {
                View.ToFavourite = "Dodano aukcje";
            }
            else
            {
                View.ToFavourite = "Juz dodana";
            }
        }

        public void ChangeCurrency()
        {
            var repo = RepoFactory.Create<IAuctionRepository>();
            ICurrencyExchangeRepository currencyRepo = new CurrencyExchangeRepository();
            var auction = repo.GetAuctionById(View.AuctionId);
            decimal prize = auction.Prize;
            View.NewPrize = currencyRepo.ConvertPrize("PLN", View.Currency, prize);
        }
    }
}