﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class AddOpinionPresenter : BasePresenter<IAddOpinionView>
    {
        public override void Load(bool isPostBack)
        {
            base.Load(isPostBack);

            View.OpinionForMyself();

        }

        public string GetUserName()
        {
            //Global.ContainerProvider.RequestLifetime.Resolve<IAuctionRepository>();
            IUserRepository userRepository = new UserRepository();
            return userRepository.GetCurrentUserName();
        }

        public Guid GetUser()
        {
            IUserRepository userRepository = new UserRepository();
            return userRepository.GetUserIdByUserName(View.UserName);

        }

        public void AddOpinion()
        {   
            IOpinionRepository opinionRepository = new OpinionRepository();
            opinionRepository.AddOpinion(View.Contents, GetUser());
        }
    }
}