﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class DefaultPresenter : BasePresenter<IDefaultView>
    {
        public override void Load(bool isPostBack)
        { 
            ICategoryRepository categoryRepository = new CategoryRepository();
            var categoryList = categoryRepository.GetAll();
            base.Load(isPostBack);
     
            if (!isPostBack)
            {
                View.PopulateTreeView(categoryList);
            }
           
        }

        public List<Auction> GetAuctions()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAll();
        }

        public List<Category> GetCategories()
        {
            ICategoryRepository categoryRepository = new CategoryRepository();
            return categoryRepository.GetAll();
        }

        //public void GetAuctionsByCategoryId(int categoryId)
        //{
        //    IAuctionRepository auctionRepository = new AuctionRepository();
        //    var auctions = auctionRepository.GetAuctionsByCategoryId(categoryId);
        //    View.DefaultDataBind(auctions);
        //}

        //public void GetAuctionsContainsUserName(string username, int wynik)
        //{
        //    IAuctionRepository auctionRepository = new AuctionRepository();
        //    var auctions = auctionRepository.GetAuctionsContains(username,wynik);
        //    View.DefaultDataBind(auctions);
        //}
    }
}