﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.UserControls;
using WebApplication5.Users;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class AuctionFormPresenter : BasePresenter<IAuctionFormView> 
    {
        public override void Load(bool isPostBack)
        {
            IAuctionRepository repo = new AuctionRepository();

            base.Load(isPostBack);
            if (!isPostBack)
            {
                View.AuctionFormBind(GetCategories());
                if (View.Mode == AuctionForm.DisplayMode.Edit) { 
                var auction = repo.GetAuctionById(View.Id);
                View.ShowAuction(auction);
                }
                else
                {
                    View.ButtonText = "Dodaj";
                }
            }
        }


        public void EditAuctionWithImage(string filename,string contenttype,Binary bytes)
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            IImageRepository repository = new ImageRepository();
            var image = repository.AddImage(filename, contenttype, bytes);
            auctionRepository.EditAuction(View.Id, View.Name, View.Description, View.Prize, View.CategoryId, image);
        }

        public void EditAuction()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            auctionRepository.EditAuction(View.Id, View.Name, View.Description, View.Prize, View.CategoryId);
        }

        public List<Category> GetCategories()
        {
            var repo = RepoFactory.Create<ICategoryRepository>();  // container.Resolve<ICategoryRepository>()
            ICategoryRepository categoryRepository = new CategoryRepository();
            return repo.GetAll();
            
        }

        public Category GetCategory(int catId)
        {
            ICategoryRepository categoryRepository = new CategoryRepository();
            return categoryRepository.GetCategoryById(catId);
        }

        public Auction GetAuctionById(int id)
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAuctionById(id);
        }

        public void AddAuction()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            auctionRepository.Add(GetUserId(),View.Name,View.Description,View.Prize,View.CategoryId);
        }


        public Guid GetUserId()
        {
            IUserRepository repository = new UserRepository();
            return repository.GetUserId();
        }

        public void AddAuctionWithImage(string filename, string contenttype, Binary bytes)
        {
            IImageRepository repository = new ImageRepository();
            var image = repository.AddImage(filename, contenttype, bytes);
            IAuctionRepository auctionRepository = new AuctionRepository();
            auctionRepository.Add(GetUserId(), View.Name, View.Description, View.Prize, View.CategoryId,image );

        }
    }
}