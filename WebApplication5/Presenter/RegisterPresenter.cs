﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class RegisterPresenter : BasePresenter<IRegisterView>
    {
        public override void Load(bool isPostBack)
        {
            base.Load(isPostBack);
            if (!isPostBack)
            {
                 View.SetSecurityQUestion();
            }
           
        }

        public string Register()
        {
            IUserRepository userRepository = new UserRepository();
            return userRepository.Register(View.UserName, View.Password, View.Email, View.SecurityQuestion,
                View.SecurityAnswer);
        }
    }
}