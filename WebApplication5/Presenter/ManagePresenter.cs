﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;
using WebApplication5.View;

namespace WebApplication5.Presenter
{
    public class ManagePresenter : BasePresenter<IManageView>
    {
        public override void Load(bool isPostBack)
        {
            base.Load(isPostBack);
            if (!isPostBack)
            {
                View.AuctionList = GetAuctionsById();
                View.ManageDataBind();
            }
        }

        public List<Auction> GetAuctionsById()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            return auctionRepository.GetAuctionsById(View.UserId);
            
        }

        public void Delete()
        {
            IAuctionRepository auctionRepository = new AuctionRepository();
            auctionRepository.RemoveAuction(auctionRepository.GetAuctionById(View.AuctionId));
        }
    }
}