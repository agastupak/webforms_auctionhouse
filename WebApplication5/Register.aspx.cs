﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5
{
    public partial class Register : BasePage<RegisterPresenter>, IRegisterView
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }

        protected void btnCreateAccount_OnClick(object sender, EventArgs e)
        {
            lblCreateAccountResults.Text = Presenter.Register();
            if (lblCreateAccountResults.Text == "Utworzono uzytkownika!")
            {
                RegisterComplete();

            }
        }

        const string SequrityQuestion = "jakies pytanie np: Jaki jest Twoj ulubiony kolor?";

        public string UserName => txtbUserName.Text;
        public string Password => txtbPassword.Text;
        public string Email => txtbEmail.Text;
        public string SecurityQuestion => lblSecurityQuestion.Text;
        public string SecurityAnswer => txtbSecurityAnswer.Text;

        public void SetSecurityQUestion()
        {
            lblSecurityQuestion.Text = SequrityQuestion;
        }

        public void RegisterComplete()
        {
            lblUserName.Visible = false;
            txtbUserName.Visible = false;

            lblPassword.Visible = false;
            txtbPassword.Visible = false;

            ConfirmPass.Visible = false;
            lblConfirmPassword.Visible = false;

            lblEmail.Visible = false;
            txtbEmail.Visible = false;

            lblSecurityQuestion.Visible = false;
            txtbSecurityAnswer.Visible = false;
        }
    }
}