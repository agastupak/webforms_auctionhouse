﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.UserControls
{
    public partial class AuctionListView : BaseUserControl<AuctionListViewPresenter>, IAuctionListView
    {
        public FilterMode FilterMode
        {
            get
            {
                var mode = FilterMode.None;

                if (cblSearch.Items.FindByValue("Username").Selected)
                {
                    mode |= FilterMode.User;
                }
                if (cblSearch.Items.FindByValue("Name").Selected)
                {
                    mode |= FilterMode.Name;
                }
                if (cblSearch.Items.FindByValue("Description").Selected)
                {
                    mode |= FilterMode.Description;
                }

                return mode;
            }
        }

        public string Keyword => txtSearch.Text;

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Bind();
        }

        public enum ALVDisplayMode
        {
            All,
            UserAuctions,
            Favourite,
            FromCategory
        }

        public ALVDisplayMode Mode { get; set; }
        public void BindAuctions(IEnumerable<Auction> auctions)
        {
            lsvAuctions.DataSource = auctions;
            lsvAuctions.DataBind();
        }

        public void HideSearchBox()
        {
            btnSearch.Visible = false;
            cblSearch.Visible = false;
            txtSearch.Visible = false;
        }

        public string Name => Request.QueryString["name"];

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            Presenter.GetAuctionsContains();
        }

    }
}

