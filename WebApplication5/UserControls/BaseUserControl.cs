﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.UserControls
{
    public class BaseUserControl<T> : UserControl, IBaseView where T : BasePresenter, new()
    {    
        public T Presenter { get; private set; }

        public BaseUserControl()
        {
            Presenter = new T();
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Presenter.Initialize(this);
        }
    }
}