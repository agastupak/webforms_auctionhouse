﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuctionForm.ascx.cs" Inherits="WebApplication5.UserControls.AuctionForm" %>

<table id="formtable" class="auto-style1">
    <tr>
        <td>
            <asp:Label ID="lblName" Text="Nazwa aukcji" runat="server"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtbName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="reqfvName" Text="*" ControlToValidate="txtbName" ErrorMessage="Wpisz nazwe"></asp:RequiredFieldValidator>
        </td>

    </tr>
    <tr>
        <td>
            <asp:Label ID="lblDescription" Text="Opis" runat="server"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtbDescription" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvDescription" Text="*" ControlToValidate="txtbDescription" ErrorMessage="Wpisz opis"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblPrize" Text="Cena" runat="server"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtbPrize" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvPrize" Text="*" ControlToValidate="txtbPrize" ErrorMessage="Wpisz cene"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPrize" runat="server" ControlToValidate="txtbPrize" ErrorMessage="zly format ceny" ValidationExpression="\d{1,8}(\,\d{1,3})?"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblCategory" runat="server" Text="Kategoria"></asp:Label></td>
        <td>
            <asp:DropDownList ID="ddlCategory" DataTextField="Name" DataValueField="Id" runat="server" AppendDataBoundItems="True">
                <asp:ListItem Text="Wybierz kategorie" Value="" />
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="rfvCategory" Text="*" ControlToValidate="ddlCategory" ErrorMessage="Wybierz kategorie"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
           <asp:FileUpload ID="FileUpload1" runat="server" /> 

        </td>
    </tr>

</table>


<asp:Button ID="btnAddAuction" OnClick="btnAddAuction_OnClick" runat="server" />

<br />
<asp:Label ID="lblMessage" runat="server"
    Font-Names="Arial"></asp:Label>

<div>
    <table>
        <tr>
            <td>
                <asp:ValidationSummary HeaderText="Prosze uzupelnic:" ID="vsAddAuction" runat="server" />
            </td>
        </tr>
    </table>
</div>

<asp:Label runat="server" ID="lblResult"></asp:Label>