﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuctionListView.ascx.cs" Inherits="WebApplication5.UserControls.AuctionListView" %>

<asp:TextBox runat="server" ID="txtSearch" ValidationGroup="czydziala"></asp:TextBox>
<asp:RequiredFieldValidator runat="server" ID="rfvSearch" ControlToValidate="txtSearch" ValidationGroup="czydziala" ErrorMessage="Wpisz czego szukac." ></asp:RequiredFieldValidator>
<asp:Button runat="server" ID="btnSearch" Text="Szukaj" OnClick="btnSearch_OnClick" />
<asp:CheckBoxList runat="server" ID="cblSearch" RepeatColumns="3">
    <asp:ListItem Text="Nick" Value="Username"></asp:ListItem>
    <asp:ListItem Text="Nazwa" Value="Name"></asp:ListItem>
    <asp:ListItem Text="Opis" Value="Description"></asp:ListItem>
</asp:CheckBoxList>
<asp:Label runat="server" ID="lblCheckError"></asp:Label>
<br />

<asp:ListView ID="lsvAuctions" runat="server">
    <LayoutTemplate>
        <table>
            <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Opis</th>
                    <th>Cena</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>

            <td><%# Eval("Name") %></td>
            <td><%# Eval("Description") %></td>
            <td><%# Eval("Prize") %></td>
            <td>
                <asp:HyperLink runat="server" ID="hlLicytuj" NavigateUrl='<%#"~/ShowDetails.aspx?id=" + Eval("Id") %>'>Licytuj</asp:HyperLink></td>
        </tr>
    </ItemTemplate>
    <EmptyDataTemplate>
        <table>
            <tr>
                <td>
                    Brak aukcji
                    <%--<asp:Label runat="server" ID="EmptyList"></asp:Label>--%>
                </td>
            </tr>
        </table>
    </EmptyDataTemplate>
</asp:ListView>
