﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Web.Security;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.UserControls
{
    public partial class AuctionForm : BaseUserControl<AuctionFormPresenter>, IAuctionFormView
    {
        public string Name
        {
            get { return txtbName.Text; }
            set { txtbName.Text = value; }
        }

        public string Description
        {
            get { return txtbDescription.Text; }
            set { txtbDescription.Text = value; }
        }

        public decimal Prize
        {
            get { return decimal.Parse(txtbPrize.Text); }
            set { txtbPrize.Text = value.ToString(); }
        }

        public int Id => int.Parse(Request.QueryString["id"]);

        public int CategoryId
        {
            get { return int.Parse(ddlCategory.SelectedValue); }
            set { ddlCategory.SelectedIndex = value; }
        }

        public Image Image { get; set; }
        public string ButtonText { set { btnAddAuction.Text = value; } }

        public void ShowAuction(Auction auction)
        {
            txtbName.Text = auction.Name;
            txtbDescription.Text = auction.Description;
            txtbPrize.Text = auction.Prize.ToString();
            ddlCategory.SelectedIndex =
            ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(auction.CategoryId.ToString()));
            btnAddAuction.Text = "Edytuj";
        }

        public void EditAuction()
        {
            Name = txtbName.Text;
            Description = txtbDescription.Text;
            Prize = decimal.Parse(txtbPrize.Text);
            CategoryId = int.Parse(ddlCategory.SelectedValue);

        }

        public void AuctionFormBind(List<Category> categories)
        {
            ddlCategory.DataSource = categories;
            ddlCategory.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }

        protected void btnAddAuction_OnClick(object sender, EventArgs e)
        {           
             // Read the file and convert it to Byte Array
                string filePath = FileUpload1.PostedFile.FileName;
                string filename = Path.GetFileName(filePath);
                string ext = Path.GetExtension(filename);
                string contenttype = String.Empty;

                //Set the contenttype based on File Extension
                switch (ext)
                {
                    case ".jpg":
                        contenttype = "image/jpg";
                        break;
                    case ".png":
                        contenttype = "image/png";
                        break;
                    case ".gif":
                        contenttype = "image/gif";
                        break;        
                }
                if (contenttype != String.Empty)
                {

                    Stream fs = FileUpload1.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    if (Mode == DisplayMode.Add)
                    {
                        //insert the file into database
                        Presenter.AddAuctionWithImage(filename, contenttype, bytes);
                        Response.Redirect("~/Users/Manage.aspx");
                    }
                    else
                    {
                        EditAuction();
                        Presenter.EditAuctionWithImage(filename, contenttype, bytes);
                        btnAddAuction.Visible = false;
                        txtbName.ReadOnly = true;
                        txtbDescription.ReadOnly = true;
                        txtbPrize.ReadOnly = true;
                        ddlCategory.Visible = false;
                        lblResult.Text = "Nowe dane zapisane";
                    }
                }
                else if (contenttype == String.Empty)
                {
                    if (Mode == DisplayMode.Add)
                    {
                        Presenter.AddAuction();
                        Response.Redirect("~/Users/Manage.aspx");
                    }
                    else
                    {
                        Presenter.EditAuction();
                    }
                }
                else{
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    lblMessage.Text = "File format not recognised." + " Upload Image formats";
                }

            }

        

        public enum DisplayMode
        {
            Add,
            Edit
        }

        public DisplayMode Mode { get; set; }

    }
}

