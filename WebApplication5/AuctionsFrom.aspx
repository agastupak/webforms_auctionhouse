﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="AuctionsFrom.aspx.cs" Inherits="WebApplication5.AuctionsFrom" %>

<%@ Register Src="~/UserControls/AuctionListView.ascx" TagPrefix="uc1" TagName="AuctionListView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CategoryMenu" runat="server">
    
    <a href="Default.aspx">Go back</a>
    <br/>
   <asp:LinkButton runat="server" ID="lbRefresh" OnClick="lbRefresh_OnClick" Text="Wszystkie"></asp:LinkButton>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <uc1:AuctionListView runat="server" ID="AuctionListView" Mode="FromCategory"/>

</asp:Content>
