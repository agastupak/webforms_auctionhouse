﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Controller
{
    public class AuctionController : ApiController
    {

        [HttpGet]
        [ActionName("GetHelloWorld")]
        public string GetHelloWorld()
        {
            var repo = RepoFactory.Create<IAuctionRepository>();
            var auction = repo.GetAll().Select(x => new {x.Name, x.Description, x.Prize, x.aspnet_User.UserId});

            return JsonConvert.SerializeObject(auction, Formatting.Indented,
                new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                });

        }
    }
}