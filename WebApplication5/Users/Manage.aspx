﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" MasterPageFile="~/MasterPageOne.Master" Inherits="WebApplication5.Users.Manage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
   
        <asp:Label runat="server" ID="lblIfNull"></asp:Label>
        
        <asp:ListView ID="lsvAuctions" runat="server">
        <LayoutTemplate>
         <table>
                <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>Opis</th>
                        <th>Cena</th>
                        <th>Kategoria</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("Name") %></td>
                <td><%# Eval("Description") %></td>
                <td><%# Eval("Prize") %></td>
                <td><%# Eval("Category.Name") %></td>
                <td><asp:LinkButton Text="Edytuj" ID="lkbEdit" CommandArgument='<%# Eval("Id") %>' OnCommand="lkbEdit_OnCommand" runat="server" /></td>
                <td><asp:LinkButton Text="Usun" ID="lkbDelete" CommandArgument='<%# Eval("Id") %>' OnCommand="lkbDelete_OnCommand" runat="server" /></td>
            </tr>
         </ItemTemplate>
                <EmptyDataTemplate>
              <table>
                <tr>     
                  <td>
                    Nie masz zadnych aukcji.
                  </td>
                </tr>
              </table>
          </EmptyDataTemplate>
    </asp:ListView>

</asp:Content>
