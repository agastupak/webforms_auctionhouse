﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="WebApplication5.Users.User" meta:resourcekey="PageResource1" %>

<%@ Register Src="~/UserControls/AuctionListView.ascx" TagPrefix="uc1" TagName="AuctionListView" %>


<asp:Content ID="Content1" ContentPlaceHolderID="CategoryMenu" runat="server">
    
  <asp:HyperLink runat="server" NavigateUrl="~/AddOpinion.aspx" meta:resourcekey="HyperLinkResource1">Dodaj opinie</asp:HyperLink>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Label runat="server" ID="lblIfNull"></asp:Label>
    
    <h3> <asp:Label runat="server" ID="lblUser" meta:resourcekey="lblUserResource1"></asp:Label>   </h3>
    <h4><asp:Label runat="server" ID="lblAuction" Text="Aukcje" meta:resourcekey="lblAuctionResource1"></asp:Label></h4>
    <uc1:AuctionListView runat="server" id="AuctionListView" Mode="UserAuctions" />
    
    <h4><asp:Label runat="server" ID="lblFavourite" Text="Ulubione" meta:resourcekey="lblFavouriteResource1"></asp:Label></h4>
    
    
    <uc1:AuctionListView runat="server" ID="AuctionListView1" Mode="Favourite" />

    <h4>Opinie</h4>
    <asp:ListView runat="server" ID="lsvOpinions">
        <LayoutTemplate>
         <table>
                <thead>
                    <tr>
                        <th>Autor</th>
                        <th>Opinia</th>
                       
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("Author") %></td>
                <td><%# Eval("Contents") %></td>
            </tr>
         </ItemTemplate>
                <EmptyDataTemplate>
              <table>
                <tr>     
                  <td>
                    Nie ma zadnych opinii.
                  </td>
                </tr>
              </table>
          </EmptyDataTemplate>
    </asp:ListView>
    

    

</asp:Content>
