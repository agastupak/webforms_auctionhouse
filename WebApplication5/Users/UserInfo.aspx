﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="WebApplication5.Users.UserInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br/>  
        <asp:Label runat="server" ID="lblIfNull"></asp:Label>
        
    
    <p>Usun uzytkownikow</p>
        <asp:ListView ID="lsvUsers" runat="server">
        <LayoutTemplate>
         <table>
                <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th></th>  
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("UserName") %></td>
                <td><asp:LinkButton Text="Usun" ID="lkbDelete" CommandArgument='<%# Eval("UserId") %>' OnCommand="lkbDelete_OnCommand" runat="server" /></td>
                </tr>
         </ItemTemplate>            
    </asp:ListView>

</asp:Content>
