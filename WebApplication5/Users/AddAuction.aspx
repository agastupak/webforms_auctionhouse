﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAuction.aspx.cs" MasterPageFile="~/MasterPageOne.Master" Inherits="WebApplication5.Users.AddAuction" %>
<%@ Register TagPrefix="aga" Namespace="WebApplication5" Assembly="WebApplication5" %>

<%@ Register Src="~/UserControls/AuctionForm.ascx" TagPrefix="uc1" TagName="AuctionForm" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
   <br />
    <uc1:AuctionForm runat="server" Mode="Add" id="AuctionForm" />

</asp:Content>
