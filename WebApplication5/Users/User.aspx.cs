﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.Users
{
    public partial class User : BasePage<UserPresenter>, IUserView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }

        public string UserName { get { return Request.QueryString["name"]; } set { lblUser.Text = value; } }
        public void UserDataBind()
        {
            lblUser.Text = UserName; ;

            lsvOpinions.DataSource = Presenter.GetOpinionsByUserName();
            lsvOpinions.DataBind();

            Session["test"] = UserName;
        }
    }
}