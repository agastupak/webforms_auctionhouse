﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAuction.aspx.cs" MasterPageFile="~/MasterPageOne.Master" Inherits="WebApplication5.Users.WebForm3" %>

<%@ Register Src="~/UserControls/AuctionForm.ascx" TagPrefix="uc1" TagName="AuctionForm" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br/>
    <uc1:AuctionForm runat="server" Mode="Edit" id="AuctionForm" />
</asp:Content>
