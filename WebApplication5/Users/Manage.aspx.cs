﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.Users
{
    public partial class Manage : BasePage<ManagePresenter>, IManageView
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);        
        }

        protected void lkbEdit_OnCommand(object sender, CommandEventArgs e)
        {
            Response.Redirect("EditAuction.aspx?id=" + Server.UrlEncode(e.CommandArgument.ToString()));
        }

        protected void lkbDelete_OnCommand(object sender, CommandEventArgs e)
        {
            AuctionId = int.Parse(e.CommandArgument.ToString());
            Presenter.Delete();
            Response.Redirect(Request.RawUrl);

        }

        public Guid UserId => (Guid)Membership.GetUser().ProviderUserKey;
        public int AuctionId { get; set; }
        public void ManageDataBind()
        {
            lsvAuctions.DataSource = AuctionList;
            lsvAuctions.DataBind();
        }

        public List<Auction> AuctionList { get; set; }
    }
}