﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5.Users
{
    public partial class UserInfo : BasePage<UserInfoPresenter>, IUserInfoView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }

        protected void lkbDelete_OnCommand(object sender, CommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            UserId = Guid.Parse(id);
            //Presenter.DeleteUser();
            Response.Redirect("UserInfo.aspx");
        }

        public Guid UserId { get; set; }
        public List<aspnet_User> UserList { get; set; }
        public void UserInfoDataBind()
        {
            lsvUsers.DataSource = UserList;
            lsvUsers.DataBind();

        }
    }
}
