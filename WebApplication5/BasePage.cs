﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5
{
    public class BasePage<T> : Page, IBaseView where T : BasePresenter, new()
    {
        public T Presenter { get; private set; }

            public BasePage()
            {
                Presenter = new T();
            }
            protected override void OnInit(EventArgs e)
            {
                base.OnInit(e);
                Presenter.Initialize(this);
            }

            protected override void OnPreLoad(EventArgs e)
            {
                base.OnPreLoad(e);
            }

            protected override void OnLoad(EventArgs e)
            {
                base.OnLoad(e);
            }

            protected override void OnLoadComplete(EventArgs e)
            {
                base.OnLoadComplete(e);
            }

            protected override void OnPreRender(EventArgs e)
            {
                base.OnPreRender(e);
            }
        

    }
}