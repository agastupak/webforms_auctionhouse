﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            IAuctionRepository repository = new AuctionRepository();
            IImageRepository imageRepository = new ImageRepository();

            var auction = repository.GetAuctionById(int.Parse(context.Request.QueryString["id"]));

            if (auction.Image!=null)
            {
                context.Response.BinaryWrite(auction.Image.Data.ToArray());
                context.Response.ContentType = auction.Image.ContentType;
            }
            else
            {
                context.Response.BinaryWrite(imageRepository.GetImageBinary(6).ToArray());
                context.Response.ContentType = imageRepository.GetImageContentType(6);
            }

        }


        public
            bool IsReusable
        {
            get;
        }
    }
}