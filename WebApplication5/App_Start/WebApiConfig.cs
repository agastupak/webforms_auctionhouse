﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace WebApplication5
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{Action}/{id}",
               defaults: new
               {
                   Controller = "Auction",
                   Action = "GetHelloWorld",
                   id = RouteParameter.Optional
               }
            );
        }
    }
}