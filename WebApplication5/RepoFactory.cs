﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5
{
    public class RepoFactory
    {      
        public static T Create<T>() where T : class
        {
            if (typeof(T)==typeof(IAuctionRepository))
            {
                return new AuctionRepository() as T;
            }
            if (typeof(T) == typeof(ICategoryRepository))
            {
                string value = System.Configuration.ConfigurationManager.AppSettings["RepoMode"];
                if (value == "XML")
                {
                      return new XmlCategoryRepository() as T;
                }
                else
                {

                    return new CategoryRepository() as T;
                }
              
            }
            if (typeof(T) == typeof(IFavouriteRepository))
            {
                return new FavouriteRepository() as T;
            }
            if (typeof(T) == typeof(IImageRepository))
            {
                return new ImageRepository() as T;
            }
            if (typeof(T) == typeof(IOpinionRepository))
            {
                return new OpinionRepository() as T;
            }
            if (typeof(T) == typeof(IUserRepository))
            {
                return new UserRepository() as T;
            }
            return null;

            
        }
    }


}