﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication5.View
{
    public interface IRegisterView
    {
        string UserName { get;}
        string Password { get; }
        string Email { get; }
        string SecurityQuestion { get; }
        string SecurityAnswer { get; }
        void SetSecurityQUestion();

        void RegisterComplete();
    }
}
