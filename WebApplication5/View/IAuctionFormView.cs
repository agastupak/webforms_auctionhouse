﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;
using WebApplication5.UserControls;

namespace WebApplication5.View
{
   public interface IAuctionFormView
    {
        string Name { get; set; }
        string Description { get; set; }
        decimal Prize { get; set; }
        int Id { get; }
        int CategoryId { get; set; }
        Image Image { get; set; }
        string ButtonText { set; }

       void ShowAuction(Auction auction);
       void EditAuction();
       void AuctionFormBind(List<Category> categories );

        AuctionForm.DisplayMode Mode { get; set; }

        
    }
}
