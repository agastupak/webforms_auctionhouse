﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.View
{
    public interface IUserInfoView
    {
        Guid UserId { get; set; }
        List<aspnet_User> UserList { get; set; }
        void UserInfoDataBind();
    }
}
