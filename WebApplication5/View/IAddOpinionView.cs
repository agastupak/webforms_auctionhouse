﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication5.View
{
    public interface IAddOpinionView
    {
        string UserName { get; }
        string Contents { get; set; }

        void OpinionForMyself();
    }
}
