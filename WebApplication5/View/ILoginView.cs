﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication5.View
{
    public interface ILoginView
    {
        string UserName { get; }
        string Password { get; }
        string SetErrorLabel { set; }
        bool RememberMe { get; }
    }
}
