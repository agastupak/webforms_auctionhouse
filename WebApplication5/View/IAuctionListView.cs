﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;
using WebApplication5.UserControls;

namespace WebApplication5.View
{
    public interface IAuctionListView
    {
        string Name { get; }
        AuctionListView.ALVDisplayMode Mode { get; set; }
        void BindAuctions(IEnumerable<Auction> auctions );
        void HideSearchBox();

        FilterMode FilterMode { get; }
        string Keyword { get; }
    }
}
