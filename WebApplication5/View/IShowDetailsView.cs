﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.View
{
    public interface IShowDetailsView
    {   aspnet_User AspnetUser { get; set; }
        string Currency { get; }
        int AuctionId { get; }
        decimal Prize { get; set; }
        void ShowAuction(Auction auction);

        void SetLinks();
        bool IsLogged { get; set; }
        void NotLogged();
        decimal NewPrize { get; set; }
        void PrizeAlert(string alert);
        string ToFavourite { set; }
    }
}
