﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.View
{
    public interface IDefaultView
    {
        //void DefaultDataBind(List<Auction> auctions);
        //void CategoriesDataBind(List<Category> categories);
        void PopulateTreeView(List<Category> categoryList );
    }
}
