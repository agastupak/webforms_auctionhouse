﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.View
{
    public interface IManageView
    {
        Guid UserId { get; }
        int AuctionId { set; get; }
        void ManageDataBind();
        List<Auction> AuctionList { get; set; }
    }
}
