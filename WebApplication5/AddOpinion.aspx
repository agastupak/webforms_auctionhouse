﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="AddOpinion.aspx.cs" Inherits="WebApplication5.AddOpinion" meta:resourcekey="PageResource1"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="CategoryMenu" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br/>
    <asp:Label runat="server" ID="lblError" meta:resourcekey="lblErrorResource1" ></asp:Label>
    <asp:TextBox runat="server" ID="txtbAddOpinion" meta:resourcekey="txtbAddOpinionResource1"></asp:TextBox>
    <asp:Button runat="server" ID="btnAdd" Text="Dodaj opinie" OnClick="btnAdd_OnClick" meta:resourcekey="btnAddResource1"/>
    <asp:Label runat="server" ID="lbl" meta:resourcekey="lblResource1" ></asp:Label>
</asp:Content>
