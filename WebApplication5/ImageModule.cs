﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebApplication5
{
    public class ImageModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
               
        }

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;

            if (application.Request.UrlReferrer == null && context.Request.PhysicalPath.Contains("ImageHandler.ashx"))
            {
               context.Response.Redirect("Default.aspx");
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }


        //HttpContext context = ((HttpApplication)sender).Context;
        //// Do nothing if the request is legal
        //if (ReguestIsLegal(context))
        //    return;

        //// Accessed directly

        //if (context.Request.UrlReferrer == null)
        //{ 
        //    context.Response.Write("Access denied");
        //    context.Response.End();
        //     HttpContext.Current.ApplicationInstance.CompleteRequest();
        //}
        //// Linked to or embedded into another domain
        //if (context.Request.UrlReferrer.Host != context.Request.Url.Host)
        //{
        //    context.Response.Write("Access denied");
        //    context.Response.End();
        //}

    


        private bool ReguestIsLegal(HttpContext context)
        {
            string mappings = ConfigurationManager.AppSettings["BlockMapping"];
            string fileName = context.Request.PhysicalPath;

            foreach (string map in mappings.Split('|'))
            {
                string cleaned = map.Replace("*", ".*").Replace(".", "\\.");
                if (Regex.IsMatch(fileName, cleaned, RegexOptions.IgnoreCase))
                    return false;
            }

            return true;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }
}