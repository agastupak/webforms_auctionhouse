﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication5.Login" culture="auto" meta:resourcekey="PageResource1" uiculture="auto"  %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <br />
    <table>
        <tr>
            <td><asp:Label runat="server" ID="lblUsername" Text="Nick" meta:resourcekey="lblUsernameResource1"></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtbUserName" meta:resourcekey="txtbUserNameResource1" ></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvUserName" ControlToValidate="txtbUserName" Text="*" ErrorMessage="Wpisz login"></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td><asp:Label runat="server" ID="lblPassword" Text="Hasło" meta:resourcekey="lblPasswordResource1" ></asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtbPassword" TextMode="Password" meta:resourcekey="txtbPasswordResource1"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvPassword" ControlToValidate="txtbPassword" Text="*" ErrorMessage="Wpisz hasło."></asp:RequiredFieldValidator>
            </td>
        </tr>
         <tr>
            <td>
                <asp:CheckBox runat="server" ID="chbRememberMe" Text="Zapamietaj mnie" meta:resourcekey="chbRememberMeResource1"></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btnLogin" Text="Log in" OnClick="btnLogin_OnClick" meta:resourcekey="btnLoginResource1"/>
            </td>
        </tr>
    </table>
    
    
        <asp:Label runat="server" ID="lblLoginError"></asp:Label>
        <asp:ValidationSummary runat="server"/>

    <br />
    Nie masz konta? <a href="Register.aspx">Kliknij aby zalozyc.</a>
    
</asp:Content>
