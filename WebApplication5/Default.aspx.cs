﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;
using System.Xml;
using System.IO;
using System.Text;
using System.Data.SqlClient;

namespace WebApplication5
{
    public partial class Default : BasePage<DefaultPresenter>, IDefaultView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);   
        }

        //protected void lbtnProba_OnCommand(object sender, CommandEventArgs e)
        //{
        //    int categoryId = int.Parse(e.CommandArgument.ToString());
        //    Presenter.GetAuctionsByCategoryId(categoryId);
        //}

        //public void DefaultDataBind(List<Auction> auctions)
        //{
        //    lsvAuctions.DataSource = auctions;
        //    lsvAuctions.DataBind();
        //}

        //public void CategoriesDataBind(List<Category> categories)
        //{
        //    rpCategories.DataSource = categories;
        //    rpCategories.DataBind();
            
        //}

        //protected void btnSearch_OnClick(object sender, EventArgs e)
        //{
        //    int wynik = 0;

        //    if (cblSearch.Items.FindByValue("Username").Selected)
        //    {
        //        wynik = wynik + 1;
        //    }
        //    if (cblSearch.Items.FindByValue("Name").Selected)
        //    {
        //        wynik = wynik + 2;
        //    }
        //    if (cblSearch.Items.FindByValue("Description").Selected)
        //    {
        //        wynik = wynik + 4;
        //    }
        //    Presenter.GetAuctionsContainsUserName(txtSearch.Text, wynik);
        //}


        public void PopulateTreeView(List<Category> categoryList)
        {
            TreeNode parentNode = null;

            foreach (Category category in categoryList)
            {
                parentNode = new TreeNode(category.Name,
                             category.Id.ToString());
                parentNode.NavigateUrl = "AuctionsFrom.aspx?name=" + category.Name;

                foreach (Auction auction in category.Auctions)
                {
                    TreeNode childNode = new TreeNode(auction.Name,
                                         auction.Id.ToString());
                    parentNode.ChildNodes.Add(childNode);
                    childNode.NavigateUrl = "ShowDetails.aspx?id=" + auction.Id;
                }

                parentNode.Collapse();
                TreeView1.Nodes.Add(parentNode);
            }
        }

 
    }
}