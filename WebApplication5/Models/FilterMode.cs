﻿using System;

namespace WebApplication5.Models
{
    [Flags]
    public enum FilterMode
    {
        None = 0,
        User = 1,
        Name = 1 << 1, // 2
        Description = 1 << 2 // 4
    }
}