﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Repository.Interfaces
{
    interface IFavouriteRepository
    {
        List<Favourite> GetFavAuctions(Guid userId);
        List<Favourite> GetFavAuctions(string userName);
        bool AddFavourite(Guid userId, int auctionId);
        void DeleteFromFavourite(Guid userId, int auctionId);
    }
}
