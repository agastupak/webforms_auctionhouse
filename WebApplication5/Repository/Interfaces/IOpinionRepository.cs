﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Repository.Interfaces
{
    interface IOpinionRepository
    {
        void AddOpinion(string contents,Guid userId);
        List<Opinion> GetOpinionsByUserName(string userName);
    }
}
