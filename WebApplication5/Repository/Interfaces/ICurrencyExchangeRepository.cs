﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication5.Repository.Interfaces
{
   public interface ICurrencyExchangeRepository
   {
       decimal ConvertPrize(string from, string to, decimal prize);



       decimal PayInEuro(decimal prize);
       decimal PayInDollar(decimal prize);
       decimal EurToPln(decimal euro);
       decimal UsdToPln(decimal usd);

   }
}
