﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using WebApplication5.Models;

namespace WebApplication5.Repository.Interfaces
{
    interface IUserRepository
    {
        Guid GetUserId();
        Guid GetUserIdByUserName(string username);
        string GetCurrentUserName();
        List<aspnet_User> UserList();
        bool IsLogged();

        string Register(string username, string password, string email, string securityQuestion, string securityAnswer);
        string LogIn(string username, string password, bool remember);
        //void DeleteUser(Guid userId);
    }
}
