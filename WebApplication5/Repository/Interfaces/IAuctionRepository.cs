﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Repository.Interfaces
{
    public interface IAuctionRepository
    {
        void Add(Guid userId, string name, string description, decimal prize, int catId);
        void Add(Guid userId,string name, string description, decimal prize, int catId, Image image);

        List<Auction> GetAll();
        List<Auction> GetAuctionsById(Guid userId);
        List<Auction> GetAuctionsByCategoryId (int categoryId);
        List<Auction> GetAuctionsByCategoryName(string name);
        List<Auction> GetAuctionsByUserName(string userName);

        Auction GetAuctionById(int id);

        void EditAuction(int id, string name, string description, decimal prize, int catId, Image image);
        void EditAuction(int id, string name, string description, decimal prize, int catId);

        bool SetPrize(int id, decimal prize);
        bool SetPrize(int id, decimal prize, string currency);

        void RemoveAuction(Auction auction);

        IEnumerable<Auction> GetAuctionsContains(string search, FilterMode mode);

    }
}
