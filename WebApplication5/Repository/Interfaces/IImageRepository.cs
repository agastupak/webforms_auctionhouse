﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication5.Models;

namespace WebApplication5.Repository.Interfaces
{
   public interface IImageRepository
   {
       Image AddImage(string name, string contentType, Binary data);
       void DeleteImage(int imageid);
       void ModifyImage(int id, string name, string contentType, Binary data);
       Binary GetImageBinary(int id);
       string GetImageContentType(int id);
   }
}
