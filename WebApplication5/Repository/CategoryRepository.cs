﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class CategoryRepository : ICategoryRepository // SQLCategoryRepository
    {
        private AuctionHouseDataContext context = null;

        public CategoryRepository()
        {
            context = new AuctionHouseDataContext();
        }

        public Category GetCategoryById(int id)
        {
            var category = context.Categories.FirstOrDefault(c => c.Id == id);
            return category;
        }

        public List<Category> GetAll()
        {
            var list = context.Categories.ToList();
            return list;
        }
    }
}