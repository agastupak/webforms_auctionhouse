﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using WebApplication5.currencyconverter;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class AuctionRepository : IAuctionRepository
    {
        private AuctionHouseDataContext context = null;

        public AuctionRepository()
        {
            context = new AuctionHouseDataContext();
        }

        public void Add(Guid UserId, string name, string description, decimal prize, int catId)
        {
            Auction auction = new Auction();
            auction.UserId = UserId;
            auction.Name = name;
            auction.Description = description;
            auction.Prize = prize;
            auction.CategoryId = catId;

            context.Auctions.InsertOnSubmit(auction);
            context.SubmitChanges();
        }

        public void Add(Guid UserId, string name, string description, decimal prize, int catId, Image image)
        {
            Auction auction = new Auction();
            auction.UserId = UserId;
            auction.Name = name;
            auction.Description = description;
            auction.Prize = prize;
            auction.CategoryId = catId;
            auction.Image = image;

            context.Auctions.InsertOnSubmit(auction);
            context.SubmitChanges();
        }

        public void Update(Auction auction)
        {
            context.Refresh(RefreshMode.KeepChanges, auction);
            context.SubmitChanges();
        }

        public List<Auction> GetAll()
        {
            var list = context.Auctions.ToList();
            return list;
        }

        public List<Auction> GetAuctionsById(Guid userId)
        {
            var list = context.Auctions.Where(a => a.UserId == userId).ToList();
            return list;
        }

        public List<Auction> GetAuctionsByCategoryId(int categoryId)
        {
            var list = context.Auctions.Where(a => a.CategoryId == categoryId).ToList();
            return list;
        }

        public List<Auction> GetAuctionsByCategoryName(string name)
        {
           var list = context.Auctions.Where(a=>a.Category.Name == name).ToList();
            return list;
        }

        public List<Auction> GetAuctionsByUserName(string userName)
        {
            var list = context.Auctions.Where(a => a.aspnet_User.UserName == userName).ToList();
            return list;
        }

        public Auction GetAuctionById(int id)
        {
            var auction = context.Auctions.FirstOrDefault(a => a.Id == id);
            return auction;
        }

        public void EditAuction(int id, string name, string description, decimal prize, int catId, Image image)
        {
            var auction = context.Auctions.FirstOrDefault(a => a.Id == id);
            var category = context.Categories.FirstOrDefault(c => c.Id == catId);
            auction.Name = name;
            auction.Description = description;
            auction.Prize = prize;
            auction.Category = category;
            auction.Image = image;

            context.Refresh(RefreshMode.KeepChanges, auction);
            context.SubmitChanges();
        }

        public void EditAuction(int id, string name, string description, decimal prize, int catId)
        {
            var auction = context.Auctions.FirstOrDefault(a => a.Id == id);
            var category = context.Categories.FirstOrDefault(c => c.Id == catId);
            auction.Name = name;
            auction.Description = description;
            auction.Prize = prize;
            auction.Category = category;

            context.Refresh(RefreshMode.KeepChanges, auction);
            context.SubmitChanges();
        }

        public bool SetPrize(int id, decimal prize)
        {
            var auction = context.Auctions.FirstOrDefault(a => a.Id == id);
            if (auction.Prize < prize)
            {
                auction.Prize = prize;
                context.SubmitChanges();
                return true;
            }
            return false;
        }



        public bool SetPrize(int id, decimal prize, string currency)
        {
            ICurrencyExchangeRepository converter = new CurrencyExchangeRepository();

            var newPrize = converter.ConvertPrize(currency, "PLN", prize);
            return SetPrize(id, newPrize);

        }

        public void RemoveAuction(Auction auction)
        {
            var image = context.Images.FirstOrDefault(i => i.Id == auction.ImageId);
            if (image != null)
            {
                context.Images.DeleteOnSubmit(image);
            }
            context.Auctions.DeleteOnSubmit(auction);
            context.SubmitChanges();
        }

        public IEnumerable<Auction> GetAuctionsContains(string search, FilterMode mode)
        {
            // mode.HasFlag(HasFlag) <- extension method    
            IEnumerable<Auction> auctions = Enumerable.Empty<Auction>();
            if (mode.HasFlag(FilterMode.User))
                //if ((mode & FilterMode.User) == FilterMode.User)
            {
                auctions = auctions.Union(context.Auctions.Where(a => a.aspnet_User.UserName.Contains(search)));
            }
            if (mode.HasFlag(FilterMode.Name))
            //if ((mode & FilterMode.Description) == FilterMode.Description)
            {
                auctions = auctions.Union(context.Auctions.Where(a => a.Description.Contains(search)));
            }
            if(mode.HasFlag(FilterMode.Description))
            //if ((mode & FilterMode.Name)== FilterMode.Name)
            {
                auctions = auctions.Union(context.Auctions.Where(a => a.Name.Contains(search)));
            }

            return auctions;

        }

        public List<Auction> GetAuctionsContains(string username)
        {
            var list = context.Auctions.Where(a => a.aspnet_User.UserName.Contains(username)).ToList();
            return list;
        }

    }
}