﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class FavouriteRepository:IFavouriteRepository
    {
        private AuctionHouseDataContext context = null;

        public FavouriteRepository()
        {
            context = new AuctionHouseDataContext();
        }

        public List<Favourite> GetFavAuctions(Guid userId)
        {
            var list = context.Favourites.Where(f => f.UserId == userId).ToList();            
            return list;
        }

        public List<Favourite> GetFavAuctions(string userName)
        {
            var list = context.Favourites.Where(f => f.aspnet_User.UserName == userName).ToList();
            return list;
        }

        public bool AddFavourite(Guid userId, int auctionId)
        {

            var temp = context.Favourites.FirstOrDefault(f => f.UserId == userId && f.AuctionId == auctionId);
            if (temp == null)
            {
                Favourite fav = new Favourite
                {
                    AuctionId = auctionId,
                    UserId = userId
                };

                context.Favourites.InsertOnSubmit(fav);
                context.SubmitChanges();

                return true;
            }
            else
            {
                return false;
            }

        }

        public void DeleteFromFavourite(Guid userId, int auctionId)
        {
            var fav = context.Favourites.FirstOrDefault(f => f.UserId == userId && f.AuctionId == auctionId);
            context.Favourites.DeleteOnSubmit(fav);

        }
    }
}