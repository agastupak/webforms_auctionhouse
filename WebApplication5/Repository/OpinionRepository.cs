﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class OpinionRepository : IOpinionRepository
    {
        private AuctionHouseDataContext context = null;

        public OpinionRepository()
        {
            context = new AuctionHouseDataContext();
        }

        public void AddOpinion(string contents, Guid userId)
        {
            Opinion opinion = new Opinion();
            opinion.UserId = userId;
            opinion.Author = Membership.GetUser().UserName;
            opinion.Contents = contents;

            context.Opinions.InsertOnSubmit(opinion);
            context.SubmitChanges();
        }

        public List<Opinion> GetOpinionsByUserName(string userName)
        {
            var list = context.Opinions.Where(o => o.aspnet_User.UserName == userName).ToList();
            return list;
        }
    }
}