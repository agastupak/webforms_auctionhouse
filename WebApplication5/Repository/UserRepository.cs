﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class UserRepository : IUserRepository
    {
        private AuctionHouseDataContext context = null;

        public UserRepository()
        {
            context = new AuctionHouseDataContext();
        }
        public Guid GetUserId()
        {
            return (Guid)Membership.GetUser().ProviderUserKey;
        }

        public Guid GetUserIdByUserName(string username)
        {
            return (Guid)Membership.GetUser(username).ProviderUserKey;
        }

        public string GetCurrentUserName()
        {
            return Membership.GetUser().UserName;
        }

        public List<aspnet_User> UserList()
        {
            var list = context.aspnet_Users.ToList();
            return list;
        }

        public bool IsLogged()
        {
            if (Membership.GetUser() == null)
            {
                return false;
            }
            return true;
        }

        public string Register(string username, string password, string email, string securityQuestion, string securityAnswer)
        {

            const string newRoleName = "User";

            if (!Roles.RoleExists(newRoleName))
            {
                Roles.CreateRole(newRoleName);
            }
            MembershipCreateStatus createStatus;

            MembershipUser newUser = Membership.CreateUser(username, password, email, securityQuestion, securityAnswer, true, out createStatus);
            switch (createStatus)
            {
                case MembershipCreateStatus.Success:
                    Roles.AddUserToRole(newUser.UserName, newRoleName);
                    return "Utworzono uzytkownika!";
                //Response.Redirect("Default.aspx");

                case MembershipCreateStatus.DuplicateUserName:
                    return "Istnieje użytkownik o podanych nicku.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Istnieje uzytkownik o podanych adresie e-mail.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Adres e-mail jest niepoprawny.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "Niepoprawna odpowiedz na pytanie.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Hasło musi się składać z co najmniej 7 znaków.";

                default:
                    return "Wystąpił błąd. Nie utworzono użytkownika.";

            }
        }

        public string LogIn(string username, string password, bool remember)
        {
            string errorMassage = null;
            // Validate the user against the Membership framework user store
            if (Membership.ValidateUser(username, password))
            {
                // Log the user into the site
                FormsAuthentication.RedirectFromLoginPage(username, remember);
            }
            else
            {
                if (Membership.GetUser(username) == null)
                {
                    errorMassage =  "nie istnieje taki uzytkownik";
                }
                else
                {
                    errorMassage =  "zle haslo";
                }
            }

            return errorMassage;
        }

        //public void DeleteUser(Guid userId)
        //{
        //    IAuctionRepository auctionRepository = new AuctionRepository();

        //    var auctions = auctionRepository.GetAuctionsById(userId);
        //    if (auctions != null)
        //    {
        //        foreach (var auction in auctions)
        //        {
        //            auctionRepository.RemoveAuction(auction);
        //        }
        //    }
        //    //var user = context.aspnet_Memberships.FirstOrDefault(u => u.UserId == userId);
        //    //context.aspnet_Memberships.DeleteOnSubmit(user);

        //    var user = context.aspnet_Users.FirstOrDefault(u => u.UserId == userId);
        //    context.aspnet_UsersInRoles.DeleteOnSubmit(user);
        //    context.aspnet_Users.DeleteOnSubmit(user);
        //    context.SubmitChanges();
        //}
    }
}