﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class ImageRepository : IImageRepository
    {
        private AuctionHouseDataContext context = null;

        public ImageRepository()
        {
            context = new AuctionHouseDataContext();
        }


        public Image AddImage(string name, string contentType, Binary data)
        {
            Image image = new Image();
            image.Name = name;
            image.ContentType = contentType;
            image.Data = data;

            context.Images.InsertOnSubmit(image);
            context.SubmitChanges();

            return image;
        }

        public void DeleteImage(int imageid)
        {
            var image = context.Images.FirstOrDefault(i => i.Id == imageid);
            context.Images.DeleteOnSubmit(image);
            context.SubmitChanges();
        }

        public void ModifyImage(int id, string name, string contentType, Binary data)
        {
            var image = context.Images.FirstOrDefault(i => i.Id == id);
            image.Name = name;
            image.ContentType = contentType;
            image.Data = data;

            context.SubmitChanges();

        }

        public Binary GetImageBinary(int id)
        {
            var image = context.Images.FirstOrDefault(i=> i.Id == id);
            return image.Data;
        }

        public string GetImageContentType(int id)
        {
            var image = context.Images.FirstOrDefault(i=> i.Id == id);
            return image.ContentType;

        }
    }
}