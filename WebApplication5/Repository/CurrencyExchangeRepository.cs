﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.currencyconverter;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class CurrencyExchangeRepository : ICurrencyExchangeRepository
    {
        Converter converter = new Converter();

        public decimal ConvertPrize(string from, string to, decimal prize)
        {
            decimal toReturn = converter.GetConversionAmount(from, to, DateTime.Now, prize);
            return decimal.Round(toReturn, 2);
        }



        public decimal PayInEuro(decimal prize)
        {
            decimal inEuro = converter.GetConversionAmount("PLN", "EUR", DateTime.Now,  prize);
            inEuro = decimal.Round(inEuro, 2);
            return inEuro;

        }

        public decimal PayInDollar(decimal prize)
        {
            decimal x = converter.GetConversionAmount("PLN", "USD",DateTime.Now , prize );
            x= decimal.Round(x, 2);
            return x;
        }

        public decimal EurToPln(decimal euro)
        {
            decimal inPLN = converter.GetConversionAmount("EUR", "PLN", DateTime.Now, euro);
            inPLN = decimal.Round(inPLN, 2);
            return inPLN;
        }

        public decimal UsdToPln(decimal usd)
        {
            decimal inPLN = converter.GetConversionAmount("USD", "PLN", DateTime.Now, usd);
            return decimal.Round(inPLN, 2);  
        }
    }
}

//new DateTime(2016,8,2)