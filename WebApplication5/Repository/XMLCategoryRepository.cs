﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using WebApplication5.Models;
using WebApplication5.Repository.Interfaces;

namespace WebApplication5.Repository
{
    public class XmlCategoryRepository : ICategoryRepository
    {
        XElement sourceFile = XElement.Load(@"C:\Users\Aga\documents\visual studio 2015\Projects\WebApplication5\WebApplication5\App_Data\Categories.xml");

        public List<Category> GetAll()
        {
            List<Category> categoryList = new List<Category>();
            foreach (XElement element in sourceFile.Descendants())
            {
                Category categoryObject = new Category();
             
                categoryObject.Name = element.Attribute("Name").Value;
                categoryObject.Id = int.Parse(element.Attribute("Id").Value);         
                categoryList.Add(categoryObject);

            }
            return categoryList;
        }

        public Category GetCategoryById(int id)
        {
            XElement currentCategory = sourceFile.Descendants().FirstOrDefault(w => w.Attribute("Id").Value == id.ToString());
            
            Category category = new Category();
            category.Id = id;
            category.Name = currentCategory.Attribute("Name").Value;

            return category;
        }
    }
}


