﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Models;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5
{
    public partial class AddOpinion : BasePage<AddOpinionPresenter>, IAddOpinionView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Presenter.Load(IsPostBack);
        }

        protected void btnAdd_OnClick(object sender, EventArgs e)
        {

            if (UserName != Presenter.GetUserName())
            {
                Presenter.AddOpinion();
            }

            Response.Redirect("Users/User.aspx?name=" + UserName);
        }

        public string UserName => (string)Session["test"];
        public string Contents { get { return txtbAddOpinion.Text; } set { txtbAddOpinion.Text = value; } }
        public void OpinionForMyself()
        {
            if (UserName == Presenter.GetUserName())
            {
                lblError.Text = "Nie mozesz sobie wystawic opinii.";
                txtbAddOpinion.Visible = false;
                btnAdd.Text = "Ok";
            }
        }
    }
}