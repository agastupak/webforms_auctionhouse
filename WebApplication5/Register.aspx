﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageOne.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebApplication5.Register" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <br />

    <table>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblUserName" Text="Nick"></asp:Label></td>

            <td>
                <asp:TextBox runat="server" ID="txtbUserName"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblPassword" Text="Haslo"></asp:Label></td>
            <td>
                <asp:TextBox runat="server" ID="txtbPassword" TextMode="Password"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="lblConfirmPassword" Text="Potwierdz haslo:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="ConfirmPass" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidator1" runat="server"
                    ControlToValidate="ConfirmPass"
                    CssClass="ValidationError"
                    ControlToCompare="txtbPassword"
                    ErrorMessage="Hasla nie sa takie same."
                    ToolTip="Password must be the same" />

                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ErrorMessage="&laquo; (Required)"
                    ControlToValidate="ConfirmPass"
                    CssClass="ValidationError"
                    ToolTip="Compare Password is a REQUIRED field">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblEmail" Text="Adres e-mail"></asp:Label></td>
            <td>
                <asp:TextBox runat="server" ID="txtbEmail"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblSecurityQuestion"></asp:Label></td>
            <td>
                <asp:TextBox runat="server" ID="txtbSecurityAnswer"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" ID="btnCreateAccount" Text="Utworz uzytkownika" OnClick="btnCreateAccount_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCreateAccountResults"></asp:Label>
            </td>
        </tr>
    </table>



</asp:Content>
