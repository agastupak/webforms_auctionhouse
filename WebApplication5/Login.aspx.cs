﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication5.Presenter;
using WebApplication5.View;

namespace WebApplication5
{
    public partial class Login : BasePage<LoginPresenter>, ILoginView
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_OnClick(object sender, EventArgs e) { 

         Presenter.Login();

        }

        public string UserName => txtbUserName.Text;
        public string Password => txtbPassword.Text;
        public string SetErrorLabel { set { lblLoginError.Text = value; } }
        public bool RememberMe => chbRememberMe.Checked;
    }

}
